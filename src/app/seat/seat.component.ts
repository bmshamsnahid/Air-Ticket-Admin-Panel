import { Component, OnInit } from '@angular/core';
import {Seat} from '../model/seat';
import {SeatService} from '../services/seat.service';
import {Airplane} from '../model/airplane';
import {AirplaneService} from '../services/airplane.service';

@Component({
  selector: 'app-seat',
  templateUrl: './seat.component.html',
  styleUrls: ['./seat.component.css']
})
export class SeatComponent implements OnInit {

  airplanes: Airplane[];
  airplane: Airplane;
  selectAirplane: Airplane;
  seats: Seat[];
  seat: Seat;
  dataReceived: boolean;

  constructor(private seatService: SeatService,
              private airplaneService: AirplaneService) {
    this.dataReceived = false;
    this.selectAirplane = new Airplane();
    this.airplaneService.getAirplanes()
      .subscribe((response) => {
        if (response.success) {
          this.airplanes = response.data;
        }
      });
  }

  ngOnInit() {
    this.seatService.getAllSeats()
      .subscribe((response) => {
        if (response.success) {
          this.seats = response.data;
          this.dataReceived = true;
        }
      });
  }

  onSelectAirplane(airplane: Airplane) {
    this.selectAirplane = airplane;
  }

  onClickSearchAirplaneSeats() {
    this.seatService.getAirplaneSeats(this.selectAirplane)
      .subscribe((response) => {
        this.seats = response.data;
        console.log(this.seats);
      });
  }

  onClickUnbookSeat(seat: Seat) {
    seat.isBooked = false;
    this.seatService.updateSeat(seat)
      .subscribe((response) => {
        if (response.success) {
          console.log('Seat un booked');
        }
      });
  }

  onClickDeleteSeat(seat: Seat) {
    this.seatService.deleteSeat(seat)
      .subscribe((response) => {
        if (response.success) {
          console.log('Seat deleted');
          this.seats = this.seats.filter((st) => {
            return st._id !== seat._id;
          });
        }
      });
  }



}
