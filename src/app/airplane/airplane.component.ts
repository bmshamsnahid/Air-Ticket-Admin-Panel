import { Component, OnInit } from '@angular/core';
import {Location} from '../model/location';
import {Airplane} from '../model/airplane';
import {AirplaneService} from '../services/airplane.service';

@Component({
  selector: 'app-airplane',
  templateUrl: './airplane.component.html',
  styleUrls: ['./airplane.component.css']
})
export class AirplaneComponent implements OnInit {

  name: string;
  description: string;
  airplanes: Airplane[];
  airplane: Airplane;
  selectedAirplane: Airplane;

  // dummy data
  airplane1: Airplane;
  airplane2: Airplane;
  airplane3: Airplane;
  airplane4: Airplane;
  airplane5: Airplane;

  constructor(private airplaneService: AirplaneService) {
    this.createDummyData();
    this.selectedAirplane = new Airplane();
    this.airplane = new Airplane();
  }

  ngOnInit() {
    this.airplaneService.getAirplanes()
      .subscribe((response) => {
        if (response.success) {
          this.airplanes = response.data;
        }
      });
  }

  onClickCreateAirplane() {
    if (this.name && this.description) {
      this.airplane.name = this.name;
      this.airplane.description = this.description;
      this.airplaneService.createAirplane(this.airplane)
        .subscribe((response) => {
          console.log(response);
          if (response.success) {
            this.airplanes.push(response.data);
          }
        });
    }
  }

  onClickTable(airplane: Airplane) {
    this.selectedAirplane = airplane;
  }

  onClickUpdateAirplane() {
    this.airplaneService.updateAirplane(this.selectedAirplane)
      .subscribe((response) => {
        if (response.success) {
          console.log('Updated the airplane.');
        }
      });
  }


  onClickDeleteAirplane () {
    this.airplaneService.deleteAirplane(this.selectedAirplane)
      .subscribe((response) => {
        if (response.success) {
          this.airplanes = this.airplanes.filter((airplane) => {
            return this.selectedAirplane._id != airplane._id;
          });
        }
      });
  }

  createDummyData () {
    this.airplane1 = new Airplane();
    this.airplane1._id = '1';
    this.airplane1.name = 'Airplane 1 name';
    this.airplane1.description = 'Airplane 1 description';

    this.airplane2 = new Airplane();
    this.airplane2._id = '2';
    this.airplane2.name = 'Airplane 2 name';
    this.airplane2.description = 'Airplane 2 description';

    this.airplane3 = new Airplane();
    this.airplane3._id = '3';
    this.airplane3.name = 'Airplane 3 name';
    this.airplane3.description = 'Airplane 3 description';

    this.airplane4 = new Airplane();
    this.airplane4._id = '4';
    this.airplane4.name = 'Airplane 4 name';
    this.airplane4.description = 'Airplane 5 description';

    this.airplane5 = new Airplane();
    this.airplane5._id = '5';
    this.airplane5.name = 'Airplane 5 name';
    this.airplane5.description = 'Airplane 5 description';
  }

}
