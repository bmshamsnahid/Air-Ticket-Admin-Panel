import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { LocationComponent } from './location/location.component';
import { AirInfoComponent } from './air-info/air-info.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { HomeComponent } from './home/home.component';
import { NavigationComponent } from './navigation/navigation.component';
import { LogoutComponent } from './logout/logout.component';
import {RouterModule} from '@angular/router';
import { AirplaneComponent } from './airplane/airplane.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SeatGenerateComponent } from './seat-generate/seat-generate.component';
import {AuthGuardService} from './services/auth-guard.service';
import {AuthService} from './services/auth.service';
import {UserService} from './services/user.service';
import {HttpModule} from '@angular/http';
import {LocationService} from './services/location.service';
import {AirplaneService} from 'app/services/airplane.service';
import {AirInfoService} from './services/air-info.service';
import {SeatService} from './services/seat.service';
import { SeatComponent } from './seat/seat.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    LocationComponent,
    AirInfoComponent,
    SignInComponent,
    SignUpComponent,
    HomeComponent,
    NavigationComponent,
    LogoutComponent,
    AirplaneComponent,
    SeatGenerateComponent,
    SeatComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: 'home', canActivate: [ AuthGuardService ], component: HomeComponent },
      { path: 'navigation', component: NavigationComponent },
      { path: 'location', canActivate: [ AuthGuardService ], component: LocationComponent },
      { path: 'airplane', canActivate: [ AuthGuardService ], component: AirplaneComponent },
      { path: 'seatGenerate', canActivate: [ AuthGuardService ], component: SeatGenerateComponent },
      { path: 'airInfo', canActivate: [ AuthGuardService ], component: AirInfoComponent },
      { path: 'seat', canActivate: [ AuthGuardService ], component: SeatComponent },
      { path: 'sign-in', component: SignInComponent },
      { path: 'sign-up', component: SignUpComponent },
      { path: 'logout', canActivate: [ AuthGuardService ], component: LogoutComponent },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: '**', redirectTo: 'home', pathMatch: 'full' }
    ])
  ],
  providers: [AuthGuardService, AuthService, UserService, LocationService, AirplaneService, AirInfoService, SeatService],
  bootstrap: [AppComponent]
})
export class AppModule { }
