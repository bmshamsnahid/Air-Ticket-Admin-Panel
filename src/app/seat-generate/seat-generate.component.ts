import { Component, OnInit } from '@angular/core';
import {Airplane} from '../model/airplane';
import {AirplaneService} from '../services/airplane.service';
import {SeatService} from '../services/seat.service';

@Component({
  selector: 'app-seat-generate',
  templateUrl: './seat-generate.component.html',
  styleUrls: ['./seat-generate.component.css']
})
export class SeatGenerateComponent implements OnInit {

  airplanes: Airplane[];
  selectedAirplane: Airplane;
  totalBusinessClassSeats: string;
  totalEconomyClassSeats: string;

  constructor(private airplaneService: AirplaneService,
              private seatService: SeatService) {}

  ngOnInit() {
    this.airplaneService.getAirplanes()
      .subscribe((response) => {
        if (response.success) {
          this.airplanes = response.data;
        }
      });
  }

  onSelectAirplane(airplane: Airplane) {
    this.selectedAirplane = airplane;
  }

  onClickCreateSeats() {
    if (typeof this.totalBusinessClassSeats !== 'undefined' && typeof this.totalEconomyClassSeats !== 'undefined') {
      const myObject: any = {};
      myObject.totalBusinessClassSeats = this.totalBusinessClassSeats;
      myObject.totalEconomyClassSeats = this.totalEconomyClassSeats;
      this.seatService.generateSeat(myObject, this.selectedAirplane)
        .subscribe((response) => {
          if (response.success) {
            console.log('Seat Generated.');
          }
        });
    }
  }

}
