import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeatGenerateComponent } from './seat-generate.component';

describe('SeatGenerateComponent', () => {
  let component: SeatGenerateComponent;
  let fixture: ComponentFixture<SeatGenerateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeatGenerateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeatGenerateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
