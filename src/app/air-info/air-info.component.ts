import { Component, OnInit } from '@angular/core';
import {AirInfo} from '../model/airInfo';
import {Airplane} from '../model/airplane';
import {Location} from '../model/location';
import {AirplaneService} from '../services/airplane.service';
import {LocationService} from '../services/location.service';
import {AirInfoService} from '../services/air-info.service';
import {AirDetail} from '../model/airDetail';

@Component({
  selector: 'app-air-info',
  templateUrl: './air-info.component.html',
  styleUrls: ['./air-info.component.css']
})
export class AirInfoComponent implements OnInit {

  airInfo: AirInfo;
  airInfos: AirInfo[];

  airDetail: AirDetail;
  airDetails: AirDetail[];

  selectedAirInfo: AirInfo;

  name: string;
  description: string;
  sourceLocationId: string;
  destinationLocationId: string;
  airplaneId: string;
  fare: string;
  dates: [string];
  rawDates: [string];
  time: string;

  airplanes: Airplane[];
  locations: Location[];

  // dummy data
  airInfo1: AirInfo;
  airInfo2: AirInfo;
  airInfo3: AirInfo;
  airInfo4: AirInfo;
  airInfo5: AirInfo;

  constructor(private airplaneService: AirplaneService,
              private locationService: LocationService,
              private airInfoService: AirInfoService) {
    this.selectedAirInfo = new AirInfo();
    this.airInfo = new AirInfo();
    this.airInfoService.getAirDetails()
      .subscribe((response) => {
        this.airDetails = response.data;
      });
  }

  ngOnInit() {
    this.airplaneService.getAirplanes()
      .subscribe((response) => {
        if (response.success) {
          this.airplanes = response.data;
        }
      });
    this.locationService.getLocations()
      .subscribe((response) => {
        if (response.success) {
          this.locations = response.data;
        }
      });
  }

  onClickCreateAirInfo() {
    if (!this.name || !this.sourceLocationId ||
    !this.destinationLocationId || !this.airplaneId ||
    !this.fare || !this.dates || !this.time) {
      console.log('Invalid data.');
    } else {
      this.airInfo.name = this.name;
      this.airInfo.description = this.description;
      this.airInfo.sourceLocationId = this.sourceLocationId;
      this.airInfo.destinationLocationId = this.destinationLocationId;
      this.airInfo.airplaneId = this.airplaneId;
      this.airInfo.fare = this.fare;
      this.airInfo.dates = this.dates;
      this.airInfo.time = this.time;
      this.airInfoService.createAirInfo(this.airInfo)
        .subscribe((response) => {
          this.airInfoService.getAirDetails()
            .subscribe((response) => {
              this.airDetails = response.data;
            });
        });
    }
  }

  onClickTable(airInfo) {
    this.selectedAirInfo = airInfo;
  }

  onSelectSource(location: Location) {
    this.sourceLocationId = location._id;
  }

  onSelectDestination(location: Location) {
    this.destinationLocationId = location._id;
  }

  onSelectAirplane(airplane: Airplane) {
    this.airplaneId = airplane._id;
  }

  onClickUpdateAirInfo() {
    this.airInfoService.updateAirInfo(this.selectedAirInfo)
      .subscribe((response) => {
        console.log(response);
        if (response.success) {
          console.log('Updated');
        }
      });
  }

  onClickDeleteAirInfo() {
    this.airInfoService.deleteAirInfo(this.selectedAirInfo)
      .subscribe((response) => {
        if (response.success) {
          this.airDetails = this.airDetails.filter((airDetail) => {
            return airDetail.airInfo._id !== response.data._id;
          });
        }
      });
  }

}
