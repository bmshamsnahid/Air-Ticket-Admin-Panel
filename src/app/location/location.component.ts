import { Component, OnInit } from '@angular/core';
import {Location} from '../model/location';
import {LocationService} from '../services/location.service';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

  name: string;
  description: string;
  locations: Location[];
  location: Location;
  selectedLocation: Location;

  constructor(private locationService: LocationService) {
    this.selectedLocation = new Location();
    this.location = new Location();
  }

  ngOnInit() {
    this.locationService.getLocations()
      .subscribe((response) => {
        if (response.success) {
          this.locations = response.data;
        }
      });
  }

  onClickCreateLocation() {
    if (this.name  && this.description) {
        this.location.name = this.name;
        this.location.description = this.description;
        this.locationService.createLocation(this.location)
          .subscribe((response) => {
            console.log(response);
            if (response.success) {
              this.locations.push(response.data);
            }
          });
    }
  }

  onClickTable(location: Location) {
    this.selectedLocation = location;
  }

  onClickUpdateLocation() {
    if (this.selectedLocation.name) {
      this.locationService.updateLocation(this.selectedLocation)
        .subscribe((response) => {
          if (response.success) {
            console.log('Location updated.');
          }
        });
    }
  }

  onClickDeleteLocation() {
    if (this.selectedLocation) {
      this.locationService.deleteLocation(this.selectedLocation)
        .subscribe((response) => {
          if (response.success) {
            this.locations = this.locations.filter((location) => {
              return this.selectedLocation._id != location._id;
            });
          }
        });
    }
  }

}
