import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {User} from '../model/user';
import {environment} from '../../environments/environment';
import {Airplane} from '../model/airplane';

@Injectable()
export class AirplaneService {

  headers = new Headers();
  options = new RequestOptions();
  currentUserObj: any;
  currentUser: User;
  token: string;

  constructor(private http: Http) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj) {
      this.currentUser = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
      this.headers.append('Authorization', this.token);
    }
    this.headers.append('Content-Type', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  createAirplane (airplane: Airplane) {
    return this.http.post(`${environment.baseUrl}/api/airplane`, JSON.stringify(airplane), this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  getAirplanes () {
    return this.http.get(`${environment.baseUrl}/api/airplane`, this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  updateAirplane (airplane: Airplane) {
    return this.http.patch(`${environment.baseUrl}/api/airplane/${airplane._id}`, JSON.stringify(airplane), this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  deleteAirplane (airplane: Airplane) {
    return this.http.delete(`${environment.baseUrl}/api/airplane/${airplane._id}`, this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

}
