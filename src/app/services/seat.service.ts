import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {User} from '../model/user';
import {environment} from '../../environments/environment';
import {Seat} from '../model/seat';
import {Airplane} from '../model/airplane';

@Injectable()
export class SeatService {

  headers = new Headers();
  options = new RequestOptions();
  currentUserObj: any;
  currentUser: User;
  token: string;

  constructor(private http: Http) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj) {
      this.currentUser = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
      this.headers.append('Authorization', this.token);
    }
    this.headers.append('Content-Type', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  createSeat (seat: Seat) {
    return this.http.post(`${environment.baseUrl}/api/seat`, JSON.stringify(seat), this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  getAllSeats () {
    return this.http.get(`${environment.baseUrl}/api/seat`, this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  getAirplaneSeats (airplane: Airplane) {
    return this.http.get(`${environment.baseUrl}/api/seat/airplane/${airplane._id}`, this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  updateSeat (seat: Seat) {
    return this.http.patch(`${environment.baseUrl}/api/seat/${seat._id}`, JSON.stringify(seat), this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  deleteSeat (seat: Seat) {
    return this.http.delete(`${environment.baseUrl}/api/seat/${seat._id}`, this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

  generateSeat(seatObject: any, airplane: Airplane) {
    return this.http.post(`${environment.baseUrl}/api/seat/airplaneSeatsGenerate/${airplane._id}`, JSON.stringify(seatObject), this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

}
