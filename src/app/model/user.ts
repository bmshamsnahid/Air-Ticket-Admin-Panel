export class User {
  constructor() {}
  _id: string;
  email: string;
  password: string;
  createdDate: string;
  isAdmin: boolean;
}
