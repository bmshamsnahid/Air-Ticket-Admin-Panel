export class Seat {
  constructor() {}
  _id: string;
  seatNo: string;
  airplaneId: string;
  isBooked: boolean;
  isBusinessClass: boolean;
}
